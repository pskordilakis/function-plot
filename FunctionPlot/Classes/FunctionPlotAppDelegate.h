//
//  FunctionPlotAppDelegate.h
//  FunctionPlot
//
//  Created by mikod on 7/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlotViewController.h"
#import "MenuViewController.h"

@interface FunctionPlotAppDelegate : NSObject <UIApplicationDelegate> {
    IBOutlet UIWindow *window;
    IBOutlet UINavigationController *nav;
}


@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet UINavigationController *nav;
@property (nonatomic, retain) MenuViewController *menuViewController;
@property (nonatomic, retain) PlotViewController *plotViewController;

-(void)plotFunction:(NSString *)function from:(NSNumber *)startingPoint to:(NSNumber *)endingPoint withPoints:(NSNumber *)points;
@end

