//
//  FunctionPlotAppDelegate.m
//  FunctionPlot
//
//  Created by mikod on 7/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "FunctionPlotAppDelegate.h"

@implementation FunctionPlotAppDelegate

@synthesize window;
@synthesize nav;
@synthesize plotViewController;
@synthesize menuViewController;

#pragma mark -
#pragma mark My Functions
-(void)plotFunction:(NSString *)function from:(NSNumber *)startingPoint to:(NSNumber *)endingPoint withPoints:(NSNumber *)pointsF {
	//NSLog(@"Delegate");
	self.plotViewController.function = function;
	self.plotViewController.startingPoint = startingPoint;
	self.plotViewController.endingPoint = endingPoint;
	self.plotViewController.originalep = endingPoint;
	self.plotViewController.originalsp = startingPoint;
	self.plotViewController.pointsF = pointsF;
	//NSLog(@"PVC = \nFunction :%s\nSP : %f\n EP : %f\n points : %f", plotViewController.function, [plotViewController.startingPoint doubleValue], [plotViewController.endingPoint doubleValue], [plotViewController.pointsF integerValue]);
	[self.nav pushViewController:(UIViewController *)plotViewController animated:YES];
	[self.plotViewController.view setNeedsDisplay];
}

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after application launch.
    PlotViewController *pVC = [[PlotViewController alloc] initWithNibName:@"PlotViewController" bundle:[NSBundle mainBundle]];
	self.plotViewController = pVC;
	[pVC release];
	
	[window addSubview:nav.view];
    [self.window makeKeyAndVisible];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
    [window release];
	[nav release];
	[menuViewController release];
	[plotViewController release];
	
    [super dealloc];
}


@end
