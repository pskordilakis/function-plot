//
//  MenuViewController.h
//  FunctionPlot
//
//  Created by mikod on 7/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface MenuViewController : UIViewController <UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource> {
	IBOutlet UIPickerView *pickerView;
	NSMutableArray *arrayFunc;
	NSString *function;
    IBOutlet UITextField *startingPointTextField;
    IBOutlet UITextField *endingPointTextField;
    IBOutlet UITextField *pointsTextField;
	NSNumber *startingPoint;
	NSNumber *endingPoint;
	NSNumber *pointsF;
}

@property (nonatomic, retain) IBOutlet UIPickerView *pickerView;
@property (nonatomic, retain) NSMutableArray *arrayFunc;
@property (nonatomic, retain) NSString *function;
@property (nonatomic, retain) IBOutlet UITextField *startingPointTextField;
@property (nonatomic, retain) IBOutlet UITextField *endingPointTextField;
@property (nonatomic, retain) IBOutlet UITextField *pointsTextField;
@property (nonatomic, retain) NSNumber *startingPoint;
@property (nonatomic, retain) NSNumber *endingPoint;
@property (nonatomic, retain) NSNumber *pointsF;

- (IBAction)draw:(id)sender;
- (IBAction)clear:(id)sender;
- (BOOL)checkBounds;
@end
