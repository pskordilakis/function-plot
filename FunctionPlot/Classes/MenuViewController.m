//
//  MenuViewController.m
//  FunctionPlot
//
//  Created by mikod on 7/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MenuViewController.h"
#import "FunctionPlotAppDelegate.h"

@implementation MenuViewController

@synthesize pickerView;
@synthesize arrayFunc;
@synthesize function;
@synthesize startingPointTextField;
@synthesize endingPointTextField;
@synthesize pointsTextField;
@synthesize startingPoint;
@synthesize endingPoint;
@synthesize pointsF;

#pragma mark -
#pragma mark MyFunctions
- (IBAction)draw:(id)sender {
	
	NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
	[f setNumberStyle:NSNumberFormatterDecimalStyle];
	self.startingPoint = [f numberFromString:startingPointTextField.text];
	self.endingPoint = [f numberFromString:endingPointTextField.text];
	self.pointsF = [f numberFromString:pointsTextField.text];
	[f release];
	
	if ([self checkBounds]) {
		FunctionPlotAppDelegate *appDelegate = (FunctionPlotAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate plotFunction:self.function from:self.startingPoint to:self.endingPoint withPoints:self.pointsF];
	}
}

- (IBAction)clear:(id)sender {
	endingPointTextField.text = @"";
	startingPointTextField.text = @"";
	pointsTextField.text = @"";
}

- (BOOL)checkBounds {
	//NSLog(@"start : %@, end : %@",self.startingPoint, self.endingPoint );
    if ([self.startingPoint doubleValue] > [self.endingPoint doubleValue]) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info" 
														message:@"starting point > ending point!"
													   delegate:nil 
											  cancelButtonTitle:@"OK" 
											  otherButtonTitles: nil];
		[alert show];
		[alert release];
		return NO;
    }
	else if ([self.function isEqualToString:@"ln(x)"] && [self.startingPoint doubleValue] < 0.0) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info" 
														message:@"natural logarithm does no take negative arguments!"
													   delegate:nil 
											  cancelButtonTitle:@"OK" 
											  otherButtonTitles: nil];
		[alert show];
		[alert release];
		return NO;
	}
	return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)theTextField {
    if (theTextField == startingPointTextField) {
        [startingPointTextField resignFirstResponder];
    }
	else if (theTextField == endingPointTextField) {
		[endingPointTextField resignFirstResponder];
	}
	else if (theTextField == pointsTextField) {
		[pointsTextField resignFirstResponder];
	}
    return YES;
}

#pragma mark -
#pragma mark pickerView Functions
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
   //do something with selected function
	self.function = [arrayFunc objectAtIndex:row];
	
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    return [arrayFunc count];
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component;
{
    return [arrayFunc objectAtIndex:row];
}


// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
/*
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {	
    [super viewDidLoad];
	
	//NSLog(@"MenuViewController viewDidLoad");
	
	arrayFunc = [[NSMutableArray alloc] init];
	[arrayFunc addObject:@"sin(x)"];
	[arrayFunc addObject:@"cos(x)"];
	[arrayFunc addObject:@"exp(x)"];
	[arrayFunc addObject:@"ln(x)"];
	[arrayFunc addObject:@"sinc(x)"];
	
	self.function = [arrayFunc objectAtIndex:0];
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
	
	[self setStartingPointTextField:nil];
	[self setEndingPointTextField:nil];
	[self setPointsTextField:nil];
    //Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[pickerView release];
	[arrayFunc release];
	[function release];
	[startingPointTextField release];
	[endingPointTextField release];
	[pointsTextField release];
	[startingPoint release];
	[endingPoint release];
	[pointsF release];
	
    [super dealloc];
}

@end
