//
//  PlotView.h
//  FunctionPlot
//
//  Created by mikod on 7/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "PlotViewController.h"
#import <math.h>

@class PlotViewController;

@interface PlotView : UIView {
	PlotViewController *pvc;
}

@property (nonatomic, retain) PlotViewController *pvc;

-(void)drawAxis;
-(void)drawCali;
-(void)drawSin;
-(void)drawCos;
-(void)drawExp;
-(void)drawLn;
-(void)drawSinc;

@end
