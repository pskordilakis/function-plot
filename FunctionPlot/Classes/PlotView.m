//
//  PlotView.m
//  FunctionPlot
//
//  Created by mikod on 7/4/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PlotView.h"
#import "PlotViewController.h"

@implementation PlotView

@synthesize pvc;

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
		[self setBackgroundColor:[UIColor blackColor]];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code.
	//NSLog(@"PV = \nFunction :%s\nSP : %f\n EP : %f\n points : %f", pvc.function, pvc.startingPoint, pvc.endingPoint, pvc.pointsF);
	[self drawAxis];
	if ([pvc.function isEqualToString:@"sin(x)"]) {
		[self drawSin];
	}
	else if ([pvc.function isEqualToString:@"cos(x)"]) {
		[self drawCos];
	}
	else if ([pvc.function isEqualToString:@"exp(x)"]) {
		[self drawExp];
	}
	else if ([pvc.function isEqualToString:@"ln(x)"]) {
		[self drawLn];
	}
	else if([pvc.function isEqualToString:@"sinc(x)"]) {
		[self drawSinc];
	}
	
	[self drawCali];
}

-(void)drawCali {
	//NSLog(@"DrawCali");
	CGFloat width = self.bounds.size.width, height = self.bounds.size.height;
	CGFloat normalizeX = -[pvc.startingPoint doubleValue], ratioX = width/([pvc.endingPoint doubleValue] - [pvc.startingPoint doubleValue]);
	CGFloat normalizeY = -[pvc.top doubleValue], ratioY = height/([pvc.down doubleValue] - [pvc.top doubleValue]);
	
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGContextSelectFont(ctx, "Courier", 15, kCGEncodingMacRoman);
	CGFloat blue[] = {0, 0, 2, 1};
	CGContextSetFillColor(ctx, blue);
	CGContextSetTextMatrix(ctx, CGAffineTransformMake(1.0, 0.0, 0.0, -1.0, 0.0, 0.0));
	
	CGFloat vimaX = ([pvc.endingPoint doubleValue] - [pvc.startingPoint doubleValue]) / 6;
	for (CGFloat counter = [pvc.startingPoint doubleValue]; counter <= [pvc.endingPoint doubleValue]; counter+=vimaX) {
		NSString *text = [NSString stringWithFormat:@"%1.1f",counter];
		CGContextShowTextAtPoint(ctx, (counter + normalizeX)*ratioX, height/2, [text cStringUsingEncoding:[NSString defaultCStringEncoding]],[text length]);
	}
	
	CGFloat vimaY = ([pvc.top doubleValue] - [pvc.down doubleValue]) / 6;
	for (CGFloat counter = [pvc.top doubleValue]; counter >= [pvc.down doubleValue]; counter-=vimaY) {
		NSString *text = [NSString stringWithFormat:@"%1.1f",counter];
		CGContextShowTextAtPoint(ctx, width/2, (counter + normalizeY)*ratioY, [text cStringUsingEncoding:[NSString defaultCStringEncoding]], [text length]);
	}
}
	
-(void)drawAxis {
	//NSLog(@"DrawAxis");
	double width = self.bounds.size.width, height = self.bounds.size.height;
	
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGContextSetLineWidth(ctx, 2.0);
	CGContextSetRGBStrokeColor(ctx, 0, 2.0, 0, 1);
	//Draw Horizontal Axis
	CGContextBeginPath(ctx);
	CGContextMoveToPoint(ctx, width/2, 0);
	CGContextAddLineToPoint(ctx, width/2, height);
	CGContextStrokePath(ctx);
	//Draw Vertical Axis
	CGContextBeginPath(ctx);
	CGContextMoveToPoint(ctx, 0, height/2);
	CGContextAddLineToPoint(ctx, width, height/2);
	CGContextStrokePath(ctx);
}

-(void)drawSin {
	//NSLog(@"DrawSin");
	//Create array of values and find min and max values
	NSMutableArray *yArray = [[NSMutableArray alloc] initWithCapacity:[pvc.pointsF intValue]], *xArray = [[NSMutableArray alloc] initWithCapacity:[pvc.pointsF intValue]];
	
	CGFloat vima = ([pvc.endingPoint doubleValue] - [pvc.startingPoint doubleValue])/[pvc.pointsF doubleValue];
	CGFloat max = 0.0, min = 0.0;
	
	for (CGFloat counter = [pvc.startingPoint doubleValue] + vima; counter <= [pvc.endingPoint doubleValue]; counter+=vima) {
		CGFloat y = sin(counter);
		[yArray addObject:[NSNumber numberWithDouble:y]];
		[xArray addObject:[NSNumber numberWithDouble:counter]];
		if ( y > max ) {
			max = y;
		}
		else if ( y < min ) {
			min = y;
		}
	}
	
	pvc.top  = [NSNumber numberWithDouble:max];
	pvc.down = [NSNumber numberWithDouble:min];
	
	//Draw plot using values from array
	CGFloat width = self.bounds.size.width, height = self.bounds.size.height;
	CGFloat normalizeX = -[pvc.startingPoint doubleValue], ratioX = width/([pvc.endingPoint doubleValue] - [pvc.startingPoint doubleValue]);
	CGFloat normalizeY = -[pvc.top doubleValue], ratioY = height/([pvc.down doubleValue] - [pvc.top doubleValue]);
	
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGContextSetRGBStrokeColor(ctx, 2.0, 0, 0, 1);
	
	NSEnumerator *yEnumerator = [yArray objectEnumerator], *xEnumerator = [xArray objectEnumerator];
	NSNumber *yNumber, *xNumber;
	
	CGFloat firstPoint = (-[[yEnumerator nextObject] doubleValue] +normalizeY)*ratioY;
	xNumber = [xEnumerator nextObject];
	
	CGContextBeginPath(ctx);
	CGContextMoveToPoint(ctx, ([xNumber doubleValue]+normalizeX)*ratioX, firstPoint);
	
	while (yNumber = [yEnumerator nextObject]) {
		xNumber = [xEnumerator nextObject];
		CGFloat x = ([xNumber doubleValue] + normalizeX)*ratioX;
		CGFloat y = (-[yNumber doubleValue] +normalizeY)*ratioY;
		 
		 //NSLog(@"counter : %f x : %f y : %f",counter, x, y);
		 CGContextAddLineToPoint(ctx, x, y);
		 CGContextStrokePath(ctx);
		 CGContextMoveToPoint(ctx, x, y);
	}
}

-(void)drawCos {
	//NSLog(@"DrawCos");
	//Create array of values and find min and max values
	NSMutableArray *yArray = [[NSMutableArray alloc] initWithCapacity:[pvc.pointsF intValue]], *xArray = [[NSMutableArray alloc] initWithCapacity:[pvc.pointsF intValue]];
	
	CGFloat vima = ([pvc.endingPoint doubleValue] - [pvc.startingPoint doubleValue])/[pvc.pointsF doubleValue];
	CGFloat max = 0.0, min = 0.0;
	
	for (CGFloat counter = [pvc.startingPoint doubleValue] + vima; counter <= [pvc.endingPoint doubleValue]; counter+=vima) {
		CGFloat y = cos(counter);
		[yArray addObject:[NSNumber numberWithDouble:y]];
		[xArray addObject:[NSNumber numberWithDouble:counter]];
		if ( y > max ) {
			max = y;
		}
		else if ( y < min ) {
			min = y;
		}
	}
	
	pvc.top  = [NSNumber numberWithDouble:max];
	pvc.down = [NSNumber numberWithDouble:min];
	
	//Draw plot using values from array
	CGFloat width = self.bounds.size.width, height = self.bounds.size.height;
	CGFloat normalizeX = -[pvc.startingPoint doubleValue], ratioX = width/([pvc.endingPoint doubleValue] - [pvc.startingPoint doubleValue]);
	CGFloat normalizeY = [pvc.top doubleValue], ratioY = height/([pvc.top doubleValue] - [pvc.down doubleValue]);
	
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGContextSetRGBStrokeColor(ctx, 2.0, 0, 0, 1);
	
	NSEnumerator *yEnumerator = [yArray objectEnumerator], *xEnumerator = [xArray objectEnumerator];
	NSNumber *yNumber, *xNumber;
	
	CGFloat firstPoint = (-[[yEnumerator nextObject] doubleValue] +normalizeY)*ratioY;
	xNumber = [xEnumerator nextObject];
	
	CGContextBeginPath(ctx);
	CGContextMoveToPoint(ctx, ([xNumber doubleValue]+normalizeX)*ratioX, firstPoint);
	
	while (yNumber = [yEnumerator nextObject]) {
		xNumber = [xEnumerator nextObject];
		CGFloat x = ([xNumber doubleValue] + normalizeX)*ratioX;
		CGFloat y = (-[yNumber doubleValue] +normalizeY)*ratioY;
		
		//NSLog(@"counter : %f x : %f y : %f",counter, x, y);
		CGContextAddLineToPoint(ctx, x, y);
		CGContextStrokePath(ctx);
		CGContextMoveToPoint(ctx, x, y);
	}
}

-(void)drawExp {
	//NSLog(@"DrawExp");
	//Create array of values and find min and max values
	NSMutableArray *yArray = [[NSMutableArray alloc] initWithCapacity:[pvc.pointsF intValue]], *xArray = [[NSMutableArray alloc] initWithCapacity:[pvc.pointsF intValue]];
	
	CGFloat vima = ([pvc.endingPoint doubleValue] - [pvc.startingPoint doubleValue])/[pvc.pointsF doubleValue];
	CGFloat max = 0.0, min = 0.0;
	
	for (CGFloat counter = [pvc.startingPoint doubleValue] + vima; counter <= [pvc.endingPoint doubleValue]; counter+=vima) {
		CGFloat y = exp(counter);
		[yArray addObject:[NSNumber numberWithDouble:y]];
		[xArray addObject:[NSNumber numberWithDouble:counter]];
		if ( y > max ) {
			max = y;
		}
		else if ( y < min ) {
			min = y;
		}
	}
	
	pvc.top  = [NSNumber numberWithDouble:max];
	pvc.down = [NSNumber numberWithDouble:min];
	
	//Draw plot using values from array
	CGFloat width = self.bounds.size.width, height = self.bounds.size.height;
	CGFloat normalizeX = -[pvc.startingPoint doubleValue], ratioX = width/([pvc.endingPoint doubleValue] - [pvc.startingPoint doubleValue]);
	CGFloat normalizeY = [pvc.top doubleValue], ratioY = height/([pvc.top doubleValue] - [pvc.down doubleValue]);
	
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGContextSetRGBStrokeColor(ctx, 2.0, 0, 0, 1);
	
	NSEnumerator *yEnumerator = [yArray objectEnumerator], *xEnumerator = [xArray objectEnumerator];
	NSNumber *yNumber, *xNumber;
	
	CGFloat firstPoint = (-[[yEnumerator nextObject] doubleValue] +normalizeY)*ratioY;
	xNumber = [xEnumerator nextObject];
	
	CGContextBeginPath(ctx);
	CGContextMoveToPoint(ctx, ([xNumber doubleValue]+normalizeX)*ratioX, firstPoint);
	
	while (yNumber = [yEnumerator nextObject]) {
		xNumber = [xEnumerator nextObject];
		CGFloat x = ([xNumber doubleValue] + normalizeX)*ratioX;
		CGFloat y = (-[yNumber doubleValue] +normalizeY)*ratioY;
		
		//NSLog(@"counter : %f x : %f y : %f",counter, x, y);
		CGContextAddLineToPoint(ctx, x, y);
		CGContextStrokePath(ctx);
		CGContextMoveToPoint(ctx, x, y);
	}
}

-(void)drawLn {
	//NSLog(@"DrawLn");
	//Create array of values and find min and max values
	NSMutableArray *yArray = [[NSMutableArray alloc] initWithCapacity:[pvc.pointsF intValue]], *xArray = [[NSMutableArray alloc] initWithCapacity:[pvc.pointsF intValue]];
	
	CGFloat vima = ([pvc.endingPoint doubleValue] - [pvc.startingPoint doubleValue])/[pvc.pointsF doubleValue];
	CGFloat max = 0.0, min = 0.0;
	
	for (CGFloat counter = [pvc.startingPoint doubleValue] + vima; counter <= [pvc.endingPoint doubleValue]; counter+=vima) {
		CGFloat y = log(counter);
		[yArray addObject:[NSNumber numberWithDouble:y]];
		[xArray addObject:[NSNumber numberWithDouble:counter]];
		if ( y > max ) {
			max = y;
		}
		else if ( y < min ) {
			min = y;
		}
	}
	
	pvc.top  = [NSNumber numberWithDouble:max];
	pvc.down = [NSNumber numberWithDouble:min];
	
	//Draw plot using values from array
	CGFloat width = self.bounds.size.width, height = self.bounds.size.height;
	CGFloat normalizeX = -[pvc.startingPoint doubleValue], ratioX = width/([pvc.endingPoint doubleValue] - [pvc.startingPoint doubleValue]);
	CGFloat normalizeY = [pvc.top doubleValue], ratioY = height/([pvc.top doubleValue] - [pvc.down doubleValue]);
	
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGContextSetRGBStrokeColor(ctx, 2.0, 0, 0, 1);
	
	NSEnumerator *yEnumerator = [yArray objectEnumerator], *xEnumerator = [xArray objectEnumerator];
	NSNumber *yNumber, *xNumber;
	
	CGFloat firstPoint = (-[[yEnumerator nextObject] doubleValue] +normalizeY)*ratioY;
	xNumber = [xEnumerator nextObject];
	
	CGContextBeginPath(ctx);
	CGContextMoveToPoint(ctx, ([xNumber doubleValue]+normalizeX)*ratioX, firstPoint);
	
	while (yNumber = [yEnumerator nextObject]) {
		xNumber = [xEnumerator nextObject];
		CGFloat x = ([xNumber doubleValue] + normalizeX)*ratioX;
		CGFloat y = (-[yNumber doubleValue] +normalizeY)*ratioY;
		
		//NSLog(@"counter : %f x : %f y : %f",counter, x, y);
		CGContextAddLineToPoint(ctx, x, y);
		CGContextStrokePath(ctx);
		CGContextMoveToPoint(ctx, x, y);
	}
}

-(void)drawSinc {
	//NSLog(@"DrawSinc");
	//Create array of values and find min and max values
	NSMutableArray *yArray = [[NSMutableArray alloc] initWithCapacity:[pvc.pointsF intValue]], *xArray = [[NSMutableArray alloc] initWithCapacity:[pvc.pointsF intValue]];
	
	CGFloat vima = ([pvc.endingPoint doubleValue] - [pvc.startingPoint doubleValue])/[pvc.pointsF doubleValue];
	CGFloat max = 0.0, min = 0.0;
	
	for (CGFloat counter = [pvc.startingPoint doubleValue] + vima; counter <= [pvc.endingPoint doubleValue]; counter+=vima) {
		CGFloat y = sin(counter)/counter;
		[yArray addObject:[NSNumber numberWithDouble:y]];
		[xArray addObject:[NSNumber numberWithDouble:counter]];
		if ( y > max ) {
			max = y;
		}
		else if ( y < min ) {
			min = y;
		}
	}
	
	pvc.top  = [NSNumber numberWithDouble:max];
	pvc.down = [NSNumber numberWithDouble:min];
	
	//Draw plot using values from array
	CGFloat width = self.bounds.size.width, height = self.bounds.size.height;
	CGFloat normalizeX = -[pvc.startingPoint doubleValue], ratioX = width/([pvc.endingPoint doubleValue] - [pvc.startingPoint doubleValue]);
	CGFloat normalizeY = [pvc.top doubleValue], ratioY = height/([pvc.top doubleValue] - [pvc.down doubleValue]);
	
	CGContextRef ctx = UIGraphicsGetCurrentContext();
	CGContextSetRGBStrokeColor(ctx, 2.0, 0, 0, 1);
	
	NSEnumerator *yEnumerator = [yArray objectEnumerator], *xEnumerator = [xArray objectEnumerator];
	NSNumber *yNumber, *xNumber;
	
	CGFloat firstPoint = (-[[yEnumerator nextObject] doubleValue] +normalizeY)*ratioY;
	xNumber = [xEnumerator nextObject];
	
	CGContextBeginPath(ctx);
	CGContextMoveToPoint(ctx, ([xNumber doubleValue]+normalizeX)*ratioX, firstPoint);
	
	while (yNumber = [yEnumerator nextObject]) {
		xNumber = [xEnumerator nextObject];
		CGFloat x = ([xNumber doubleValue] + normalizeX)*ratioX;
		CGFloat y = (-[yNumber doubleValue] +normalizeY)*ratioY;
		
		//NSLog(@"counter : %f x : %f y : %f",counter, x, y);
		CGContextAddLineToPoint(ctx, x, y);
		CGContextStrokePath(ctx);
		CGContextMoveToPoint(ctx, x, y);
	}
}

- (void)dealloc {
	[pvc release];
	
    [super dealloc];
}

@end