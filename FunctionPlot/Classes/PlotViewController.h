//
//  PlotViewController.h
//  FunctionPlot
//
//  Created by mikod on 7/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlotView.h"

@interface PlotViewController : UIViewController {
	PlotView *plotView;
	NSString *function;
	NSNumber *startingPoint;
	NSNumber *endingPoint;
	NSNumber *pointsF;
	NSNumber *top;
	NSNumber *down;
	NSNumber *originalep;
	NSNumber *originalsp;
}

@property (nonatomic, retain) PlotView *plotView;
@property (nonatomic, retain) NSString *function;
@property (nonatomic, retain) NSNumber *startingPoint;
@property (nonatomic, retain) NSNumber *endingPoint;
@property (nonatomic, retain) NSNumber *pointsF;
@property (nonatomic, retain) NSNumber *top;
@property (nonatomic, retain) NSNumber *down;
@property (nonatomic, retain) NSNumber *originalep;
@property (nonatomic, retain) NSNumber *originalsp;

-(void)dragRecognized:(UIPanGestureRecognizer *)sender;
-(void)pinchRecognized:(UIPinchGestureRecognizer *)sender;
@end
