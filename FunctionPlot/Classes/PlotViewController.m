//
//  PlotViewController.m
//  FunctionPlot
//
//  Created by mikod on 7/2/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "PlotViewController.h"


@implementation PlotViewController

@synthesize plotView;
@synthesize function;
@synthesize startingPoint;
@synthesize endingPoint;
@synthesize pointsF;
@synthesize top;
@synthesize down;
@synthesize originalep;
@synthesize originalsp;


#pragma mark -
#pragma mark My Functions

-(void) dragRecognized:(UIPanGestureRecognizer *)sender {
	CGFloat width = self.view.bounds.size.width;
	CGFloat normalizeX = -[self.startingPoint doubleValue];
	CGFloat ratioX = width/([self.endingPoint doubleValue] - [self.startingPoint doubleValue]);
	if ([sender state]==UIGestureRecognizerStateChanged) {
		
		CGPoint translate = [sender translationInView:self.view];
		CGPoint velocity = [sender velocityInView:self.view];
		
		//NSLog([NSString stringWithFormat:@"translate.x : %1.1f", translate.x]);
		
		if (velocity.x > 0.0 ) { //Drag Right
			CGFloat sp = [self.originalsp doubleValue];
			sp += abs((translate.x + normalizeX)/ratioX);
			self.startingPoint = [[NSNumber alloc] initWithDouble:sp];
			
			CGFloat ep = [self.originalep doubleValue];
			ep += abs((translate.x + normalizeX)/ratioX);
			self.endingPoint = [[NSNumber alloc] initWithDouble:ep];
			[self.view setNeedsDisplay];
		}
		else { //drag left
			CGFloat sp = [self.originalsp doubleValue];
			sp -= abs((translate.x + normalizeX)/ratioX);
			self.startingPoint = [[NSNumber alloc] initWithDouble:sp];
			
			CGFloat ep = [self.originalep doubleValue];
			ep -= abs((translate.x + normalizeX)/ratioX);
			self.endingPoint = [[NSNumber alloc] initWithDouble:ep];
			[self.view setNeedsDisplay];
		}
	}
	else if ([sender state] == UIGestureRecognizerStateEnded) {
		self.originalsp = self.startingPoint;
		self.originalep = self.endingPoint;
	}
}

-(void)pinchRecognized:(UIPinchGestureRecognizer *)sender {
	
	if ([sender state] == UIGestureRecognizerStateChanged) {
		CGFloat scale = sender.scale;
		CGFloat velocity = sender.velocity;
		
		//NSLog([NSString stringWithFormat:@"scale : %1.5f, vel : %1.5f", scale, velocity]);
		
		if (velocity > 0.0 ) { //Zoom In
			if (scale < 0.0) {
				CGFloat sp = [self.originalsp doubleValue];
				sp *=  scale;
				self.startingPoint = [[NSNumber alloc] initWithDouble:sp];
				
				CGFloat ep = [self.originalep doubleValue];
				ep *= scale;
				self.endingPoint = [[NSNumber alloc] initWithDouble:ep];
				[self.view setNeedsDisplay];
			}
			else {
				CGFloat sp = [self.originalsp doubleValue];
				sp /=  scale;
				self.startingPoint = [[NSNumber alloc] initWithDouble:sp];
				
				CGFloat ep = [self.originalep doubleValue];
				ep /= scale;
				self.endingPoint = [[NSNumber alloc] initWithDouble:ep];
				[self.view setNeedsDisplay];
			}			
		}
		else { //Zoom Out
			if (scale > 0.0) {
				CGFloat sp = [self.originalsp doubleValue];
				sp /=  scale;
				self.startingPoint = [[NSNumber alloc] initWithDouble:sp];
				
				CGFloat ep = [self.originalep doubleValue];
				ep /= scale;
				self.endingPoint = [[NSNumber alloc] initWithDouble:ep];
				[self.view setNeedsDisplay];
			}
			else {
				CGFloat sp = [self.originalsp doubleValue];
				sp *=  scale;
				self.startingPoint = [[NSNumber alloc] initWithDouble:sp];
				
				CGFloat ep = [self.originalep doubleValue];
				ep *= scale;
				self.endingPoint = [[NSNumber alloc] initWithDouble:ep];
				[self.view setNeedsDisplay];
			}
		}
	}
	else if ([sender state] == UIGestureRecognizerStateEnded) {
		self.originalsp = self.startingPoint;
		self.originalep = self.endingPoint;
	}
}

// The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization.
		self.plotView = [[PlotView alloc] init];
		plotView.pvc = self;
		self.view = plotView;
		
		UIPanGestureRecognizer *drag = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(dragRecognized:)];
		[self.view addGestureRecognizer:drag];
		[drag release];
		
		UIPinchGestureRecognizer *pinch = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchRecognized:)];
		[self.view addGestureRecognizer:pinch];
		[pinch release];
    }
    return self;
}

/*- (void)handleGesture:(UIGestureRecognizer *)sender {
	
}*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
	if (interfaceOrientation == UIInterfaceOrientationPortrait) {
		return YES;
	}
	else if (interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
		return YES;
	}
	else if (interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
		return YES;
	}
	return NO;
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[self.plotView release];
	[self.function release];
	[self.startingPoint release];
	[self.endingPoint release];
	[self.pointsF release];
	
    [super dealloc];
}


@end
